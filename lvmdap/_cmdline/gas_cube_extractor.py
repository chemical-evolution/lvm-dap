import sys
import os
import itertools
import numpy as np
import argparse
import matplotlib.pyplot as plt

from pprint import pprint
from scipy.interpolate import griddata
from scipy.ndimage import median_filter
from pyFIT3D.common.constants import __sigma_to_FWHM__
from pyFIT3D.common.tools import rss_seg2cube, get_slice, smooth_spec_clip_cube, spec_extract_cube_mean
from pyFIT3D.common.io import array_to_fits, print_done, print_block_init
from pyFIT3D.common.io import get_wave_from_header
from pyFIT3D.common.tools import flux_elines_cube_EW

from copy import deepcopy as copy
from astropy.io import fits

from astropy.visualization import make_lupton_rgb

from lvmdap._cmdline.dap import N_MC


CWD = os.getcwd()
SLICE_CONFIG_PATH = "../../_fitting-data/_configs/slice_V.conf"
EMISSION_LINES_LIST = "../../_fitting-data/_configs/MaNGA/emission_lines_momana.txt"
HALPHA_WL = 6562.85
CUBE_PATTERN = ".cube.fits.gz"

def _no_traceback(type, value, traceback):
  print(value)

def get_cs_map(filename, good_pix_mask):
    cs = fits.open(filename)

    yo, xo = np.where(cs[0].data>0)
    values = cs[0].data[yo,xo]

    yi, xi = np.where(cs[0].data<9999)

    seg_map__yx = griddata(np.column_stack((xo,yo)), values, np.column_stack((xi,yi)), method="nearest").reshape(cs[0].data.shape)*good_pix_mask.astype(int)

    return seg_map__yx

def read_fit_elines_rss(filename):
    iseg = 0
    wave__s, flux__s, eflux__s, vel__s, evel__s, sig__s, esig__s = [], [], [], [], [], [], []
    mtypes, wlc, flx, e_flx, sig, e_sig, vel, e_vel = [], [], {}, {}, {}, {}, {}, {}
    with open(filename) as f:
        for line in f:
            if line.startswith("#"):
                last_seg = iseg
                iseg = int(line.split()[-1])
                if iseg != last_seg:
                    mtypes, wlc, flx, e_flx, sig, e_sig, vel, e_vel = [], [], {}, {}, {}, {}, {}, {}
            nmod, _ = tuple(eval(v) for v in f.readline()[:-1].split())
            for _ in range(nmod):
                line_model = f.readline()[:-1].split()
                mtype = line_model[0]
                if mtype == "eline":
                    mtypes.append(mtype)
                    wl = eval(line_model[1])
                    wlc.append(wl)
                    
                    flx[wl] = eval(line_model[3])
                    e_flx[wl] = eval(line_model[4])
                    sig[wl] = eval(line_model[5])
                    e_sig[wl] = eval(line_model[6])
                    vel[wl] = eval(line_model[7])
                    e_vel[wl] = eval(line_model[8])
            wave__s.append(wlc)
            flux__s.append(flx)
            eflux__s.append(e_flx)
            vel__s.append(vel)
            evel__s.append(e_vel)
            sig__s.append(sig)
            esig__s.append(e_sig)

    ns = last_seg + 1
    wave__m = np.unique(np.concatenate(wave__s))
    flux__ms = np.zeros((wave__m.size, ns))
    eflux__ms = np.zeros((wave__m.size, ns))
    sig__ms = np.zeros((wave__m.size, ns))
    esig__ms = np.zeros((wave__m.size, ns))
    vel__ms = np.zeros((wave__m.size, ns))
    evel__ms = np.zeros((wave__m.size, ns))

    for iwl in range(wave__m.size):
        for iseg in range(ns):
            flux__ms[iwl, iseg] = flux__s[iseg].get(wave__m[iwl], 0.0)
            eflux__ms[iwl, iseg] = eflux__s[iseg].get(wave__m[iwl], 0.0)
            sig__ms[iwl, iseg] = sig__s[iseg].get(wave__m[iwl], 0.0)
            esig__ms[iwl, iseg] = esig__s[iseg].get(wave__m[iwl], 0.0)
            vel__ms[iwl, iseg] = vel__s[iseg].get(wave__m[iwl], 0.0)
            evel__ms[iwl, iseg] = evel__s[iseg].get(wave__m[iwl], 0.0)
    
    return wave__m, flux__ms, eflux__ms, vel__ms, evel__ms, sig__ms, esig__ms

def get_gas_cube(org_cube__wyx, org_wave__w, out_rss__tsw, wave_rss__w, seg_map__yx, label, slice_conf):
    slice_prefix = f'img_{label}'
    slices = get_slice(copy(org_cube__wyx), org_wave__w, slice_prefix, slice_conf)
    V__yx = list(slices.values())[0]
    V__yx[np.isnan(V__yx)] = -1
    mV__yx = median_filter(V__yx, size=(2, 2), mode='reflect')

    org_rss__sw, _, _, _, _ = spec_extract_cube_mean(copy(org_cube__wyx), seg_map__yx)
    cube__wyx = rss_seg2cube(org_rss__sw, seg_map__yx)

    slice_prefix = f'SEG_img_{label}'
    slices = get_slice(copy(cube__wyx), wave_rss__w, slice_prefix, slice_conf)
    V_slice__yx = list(slices.values())[0]
    V_slice__yx[np.isnan(V_slice__yx)] = -1
    scale_seg__yx = np.divide(mV__yx, V_slice__yx, where=V_slice__yx!=0, out=np.zeros_like(V_slice__yx))

    rsp_mod_tmp__wyx = rss_seg2cube(copy(out_rss__tsw[1]), seg_map__yx)
    rsp_mod_cube__wyx = rsp_mod_tmp__wyx*scale_seg__yx

    # clean rsp cube
    mask_negative = rsp_mod_cube__wyx<0
    negative_fluxes = np.zeros(2*mask_negative.sum())
    negative_fluxes[:negative_fluxes.size//2] = rsp_mod_cube__wyx[mask_negative].ravel()
    negative_fluxes[negative_fluxes.size//2:] = -1*rsp_mod_cube__wyx[mask_negative].ravel()
    sigma_clip = 3*negative_fluxes.std()
    mask_clean = rsp_mod_cube__wyx>sigma_clip
    rsp_mod_cube__wyx = rsp_mod_cube__wyx * mask_clean

    tmp_cube__wyx = org_cube__wyx - rsp_mod_cube__wyx

    # smooth
    smooth_cube__wyx = smooth_spec_clip_cube(copy(tmp_cube__wyx), wavebox_width=75, sigma=1.5, wavepix_min=10, wavepix_max=1860)

    # generate GAS rss input
    gas_cube__wyx = tmp_cube__wyx - smooth_cube__wyx

    return gas_cube__wyx, rsp_mod_cube__wyx, scale_seg__yx

def _main(cmd_args=sys.argv[1:]):
    parser = argparse.ArgumentParser(
        description="Run gas cube extraction from RSS analysis"
    )
    parser.add_argument(
        "--pointing",
        help=f"optional pointing for which gas cube will be extracted. If not given, run analysis on all cubes found"
    )
    parser.add_argument(
        "-i", "--input-path", metavar="path",
        help=f"path to the inputs. Defaults to '{CWD}'",
        default=CWD
    )
    parser.add_argument(
        "-p", "--dataproducts-path", metavar="path",
        help=f"path where to find CS maps. Defatuls to '{CWD}'",
        default=CWD
    )
    parser.add_argument(
        "-o", "--output-path", metavar="path",
        help=f"path to the analyzed RSS spectra and where to save outputs of this script. Defaults to '{CWD}'",
        default=CWD
    )
    parser.add_argument(
        "--slice-config-file", metavar="filename",
        help=f"filename of the slice configuration file. Defaults to {SLICE_CONFIG_PATH}",
        default=SLICE_CONFIG_PATH
    )
    parser.add_argument(
        "--elines-list-file", metavar="filename",
        help=f"filename of the emission lines list to use in moment analysis. Defaults to {EMISSION_LINES_LIST}",
        default=EMISSION_LINES_LIST
    )
    parser.add_argument(
        "-n", "--n-mc", type=int,
        help=f"number of MC realisations for the moment analysis. Defaults to {N_MC}",
        default=N_MC
    )
    parser.add_argument(
        "--overwrite",
        help="whether to overwrite output files or not (default)",
        action="store_true"
    )
    parser.add_argument(
        "-v", "--verbose",
        help="if given, shows information about the progress of the script",
        action="store_true"
    )
    parser.add_argument(
        "-d", "--debug",
        help="run in debugging mode",
        action="store_true"
    )
    args = parser.parse_args(cmd_args)
    
    if not args.debug:
        sys.excepthook = _no_traceback
    else:
        pprint("COMMAND LINE ARGUMENTS")
        pprint(f"{args}\n")


    if args.pointing is not None:
        org_cubes_path = sorted([os.path.join(root,file) for root, _, files in os.walk(args.input_path) for file in files if file.startswith(args.pointing) and file.endswith(CUBE_PATTERN)])
    else:
        org_cubes_path = sorted([os.path.join(root, file) for root, _, files in os.walk(args.input_path) for file in files if file.endswith(CUBE_PATTERN)])

    for cube_path in org_cubes_path:
        label = os.path.basename(cube_path).replace(CUBE_PATTERN, "") if args.pointing is None else args.pointing
        cs_paths = [os.path.join(args.dataproducts_path, label, f"cont_seg.{label}.fits.gz"), os.path.join(args.dataproducts_path, f"cont_seg.{label}.fits.gz")]
        out_rss_path = os.path.join(args.output_path, f"output.{label}.fits.gz")
        elines_path = os.path.join(args.output_path, f"elines_{label}")

        cs_paths_exists = list(filter(lambda path: os.path.isfile(path), cs_paths))
        if len(cs_paths_exists) == 0:
            print(f"CS map file for cube {label} is missing")
            continue
        else:
            cs_path = cs_paths_exists[0]
        if not os.path.isfile(out_rss_path):
            print(f"output RSS for cube {label} is missing")
            continue
        if not os.path.isfile(elines_path):
            print(f"output elines for cube {label} is missing")
            continue

        # read original cube
        cube = fits.open(cube_path, memmap=False)
        org_cube__wyx = np.nan_to_num(cube[0].data)
        org_wave__w = get_wave_from_header(cube[0].header, wave_axis=3)
        mask = (org_cube__wyx!=0).any(axis=0)

        # read RSS fitting output
        out_rss = fits.open(out_rss_path, memmap=False)
        wave_rss__w = get_wave_from_header(out_rss[0].header)
        out_rss__tsw = out_rss[0].data

        # read segmentation map
        seg_map__yx = get_cs_map(cs_path, good_pix_mask=mask)
        # generate RSS of emission lines fitting
        wave__m, _, _, vel__ms, _, sig__ms, _ = read_fit_elines_rss(elines_path)

        vel_map__yx = vel__ms[np.where(wave__m==HALPHA_WL)].ravel()[seg_map__yx.astype(int)-1]
        vel_map__yx[seg_map__yx==0] = 0
        sig_map__yx = sig__ms[np.where(wave__m==HALPHA_WL)].ravel()[seg_map__yx.astype(int)-1]
        sig_map__yx[seg_map__yx==0] = 0
        # compute gas cube
        gas_cube__wyx, rsp_mod_cube__wyx, dez_map__yx = get_gas_cube(org_cube__wyx, org_wave__w, out_rss__tsw, wave_rss__w, seg_map__yx, label, slice_conf=args.slice_config_file)
        
        # run moment analysis
        mom_cube__wyx, mom_header = flux_elines_cube_EW(
            flux__wyx=gas_cube__wyx,
            input_header=cube[0].header,
            n_MC=args.n_mc,
            elines_list=args.elines_list_file,
            vel__yx=vel_map__yx, sigma__yx=sig_map__yx,
            flux_ssp__wyx=rsp_mod_cube__wyx, eflux__wyx=cube[1].data
        )
        nlines = mom_cube__wyx.shape[0]//8
        flux_elines_I0 = np.nan_to_num(mom_cube__wyx[:nlines], nan=9999)
        OIII_image = flux_elines_I0[0]
        Ha_image = flux_elines_I0[1]
        SII_image = (flux_elines_I0[2]+flux_elines_I0[3])/2
        # r = 6584 (NII)
        rgb_image = make_lupton_rgb(SII_image, Ha_image, OIII_image, stretch=1, Q=0)

        plt.figure(figsize=(10,10))
        plt.imshow(rgb_image, origin="lower")
        plt.savefig(os.path.join(args.output_path, f"{label}-RGB.jpeg"), bbox_inches="tight")

        # write cubes
        gas_cube_path = os.path.join(args.output_path, f"{label}-gas.cube.fits.gz")
        rsp_cube_path = os.path.join(args.output_path, f"{label}-rsp.cube.fits.gz")
        mom_cube_path = os.path.join(args.output_path, f"{label}-moments.cube.fits.gz")
        dez_map_path = os.path.join(args.output_path, f"{label}-dezonification.map.fits.gz")
        if not os.path.isfile(gas_cube_path) or (os.path.isfile(gas_cube_path) and args.overwrite): array_to_fits(gas_cube_path, gas_cube__wyx, header=cube[0].header, overwrite=args.overwrite)
        if not os.path.isfile(rsp_cube_path) or (os.path.isfile(rsp_cube_path) and args.overwrite): array_to_fits(rsp_cube_path, rsp_mod_cube__wyx, header=cube[0].header, overwrite=args.overwrite)
        if not os.path.isfile(mom_cube_path) or (os.path.isfile(mom_cube_path) and args.overwrite): array_to_fits(mom_cube_path, mom_cube__wyx, mom_header, overwrite=args.overwrite)
        if not os.path.isfile(dez_map_path) or (os.path.isfile(dez_map_path) and args.overwrite): array_to_fits(dez_map_path, dez_map__yx, overwrite=args.overwrite)
