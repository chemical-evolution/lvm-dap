
import sys
import os
import numpy as np
import argparse

from pprint import pprint
from tqdm import tqdm
from specutils import Spectrum1D
from specutils.manipulation import gaussian_smooth
from astropy.io import fits
import astropy.units as u

from pyFIT3D.common.tools import get_wave_from_header


CWD = os.getcwd()
FWHM_MUSE = 2.2
FWHM_NEW = 6.0

def _no_traceback(type, value, traceback):
  print(value)

def _main(cmd_args=sys.argv[1:]):
    parser = argparse.ArgumentParser(
        description="Run RSS preprocessing of MUSE data"
    )
    parser.add_argument(
        "--sigma-inst", type=np.float,
        help=f"the target instrumental dispersion to downgrade the input spectra to. Defaults to {FWHM_NEW}AA",
        default=FWHM_NEW
    )
    parser.add_argument(
        "-i", "--input-path", metavar="path",
        help=f"path to the inputs. Defaults to '{CWD}'",
        default=CWD
    )
    parser.add_argument(
        "-o", "--output-path", metavar="path",
        help=f"path to the outputs. Defaults to '{CWD}'",
        default=CWD
    )
    parser.add_argument(
        "-v", "--verbose",
        help="if given, shows information about the progress of the script. Defaults to false",
        action="store_true"
    )
    parser.add_argument(
        "-d", "--debug",
        help="debugging mode. Defaults to false.",
        action="store_true"
    )
    args = parser.parse_args(cmd_args)
    
    if not args.debug:
        sys.excepthook = _no_traceback
    else:
        pprint("COMMAND LINE ARGUMENTS")
        pprint(f"{args}\n")
    
    res_correction = np.sqrt(args.sigma_inst**2 - FWHM_MUSE**2)/2.355

    sed_rss = sorted([os.path.join(root,file) for root, _, files in os.walk(args.input_path) for file in files if file.startswith("CS.LMC") and file.endswith(".RSS.fits.gz")])
    err_rss = [os.path.join(os.path.dirname(file), f"e_{os.path.basename(file)}") for file in sed_rss]

    no_error_file = False
    for ipoint in tqdm(range(len(sed_rss)), desc="writing spectra", ascii=True, unit="SED"):
        f = fits.open(sed_rss[ipoint], memmap=False)
        if not os.path.isfile(err_rss[ipoint]):
            no_error_file = True
        else:
            e = fits.open(err_rss[ipoint], memmap=False)

        wl = get_wave_from_header(f[0].header)
        dummy_err = np.abs(1e-7*np.ones_like(wl))
        
        sed_rss_cor, err_rss_cor = [], []
        for ised in range(f[0].data.shape[0]):
            sed_rss_cor.append(gaussian_smooth(Spectrum1D(spectral_axis=wl * u.AA, flux=f[0].data[ised] * u.erg/u.s/u.AA), stddev=res_correction/np.diff(wl)[0]).flux.value)
            err_rss_cor.append(dummy_err if no_error_file else np.where(e[0].data[ised]>0, e[0].data[ised], dummy_err))

        no_error_file = False
    
        f[0].data = np.asarray(sed_rss_cor)
        e[0].data = np.asarray(err_rss_cor)
        f.writeto(os.path.join(args.output_path,os.path.basename(sed_rss[ipoint])), overwrite=True)
        e.writeto(os.path.join(args.output_path,os.path.basename(err_rss[ipoint])), overwrite=True)
